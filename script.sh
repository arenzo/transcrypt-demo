#!/bin/bash
#This file should be readable unencrypted.

echo "This script will print the contents of secret files!!"
echo "======================================================"
source secretfile1.secret.sh
source secretfile2.secret.sh

echo "The first secret is: $Secret1"
echo "The second secret is: $Secret2"
echo "The third secret is: ${Secret3[*]}"

echo "======================================================"
echo "End of Execution"
