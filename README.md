# TransCrypt - POC
This repo is a proof of concept for using TransCrypt repository encryption with basic shell script execution.

The transcript password for this repo is: `cobblestone racing car tugboat`

To decrypt the files in this repo, execute the following command:

`transcript -c aes-256-cbc -p cobblestone racing car tugboat`
